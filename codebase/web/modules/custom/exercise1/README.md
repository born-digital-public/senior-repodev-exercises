# Make a simple Self Deposit Form for the Repository Item content type

## Client request

The client wants to allow students to self-submit Theses and Dissertations.

Requirements:
 * The form must be accessible at `/self-deposit` to anonymous users, and form submission must be allowed for anonymous users (no need for captcha/security for this toy example).
 * The form must have required fields for all the required fields in Repository Item's default form.  It must also have an optional field for Description. 
 * The form must have a required upload widget for a PDF file as well.
 * Upon submission:
    * Create a Repository Item node and set its appropriate fields from the form state
    * Create a media to hold the PDF, give it a 'Media Use' term of Original File, and relate it to the node via the 'Media Of' entity reference. 
    * Items submitted through the Self Deposit Form should be marked as unpublished.

Definition of done:
 * The form is accessible to all site users regardless of role.
 * Required fields are visually indicated.
 * Submission of the form is successful when required fields are populated.
 * A node of type Repository Item is created with the values in the form mapped to corresponding fields.
 * Uploading of files of the type PDF is allowed, and the uploaded file is attached to the new node and tagged as "Media Use: Original File"
 * The new node is not published, and does not show up in search results until an administrator publishes it.
