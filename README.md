# Senior Digital Repository Developer Code Exercises (Islandora flavor)
These exercises are designed to assess your Drupal knowledge and see how you solve problems.

Please don't spend more than three hours on the exercise, and if you have trouble completing it in that time don't stress about it - we don't expect everything to be completed. Work in progress code is totally fine, as long as the direction can be explained during the interview. The setup itself may take up to 30 minutes depending on the speed of your local development machine. If you'd prefer to spend all the time on only one of the exercises and flesh it out more, that is also fine.

_Note: The exercises are designed in the form of client requests, but in actual practice we do not expect task work to be done in isolation. Our process is collaborative, and you would have many opportunities to discuss requirements and possible implementation directions with your team and the client._

_Acknowledgement: Born-Digital has shamelessly borrowed the structure of these exercises from our friends and neighbors at [Last Call Media](https://github.com/LastCallMedia/srdev-d9-exercises). Thank you, Kelly & Co!_

## Instructions

- Read the exercises below
- Clone this repository locally and perform the exercises.
  -  Use the setup instructions below to boot up an Islandora ecosystem to work within. It relies on Docker to handle all of the ancillary components with minimal impact on your development machine. This repository is a close sibling of [ISLE-DC](https://github.com/Islandora-Devops/isle-dc), which is the infrastructure toolkit Born-Digital uses to run Islandora in local and remote environments.
- Push your work up in a new, _private_ repository in your own Gitlab account.
- Invite the email `drupal@born-digital.com` as a Guest member in your private repository.

## Setup instructions
- If you don't have it already, install [Docker Desktop](https://www.docker.com/products/docker-desktop/). Other local development environment requirements:
  - You must be able to run `make` commands because we have included a `Makefile` which allows complex actions to be executed with a single command.
  - It shouldn't matter if you have `docker-compose` version 1.x or 2.x.
- Navigate to the project on the command line and run `make local` and select "Generate new secrets" (option 1) when prompted.
  - You may be prompted a few times for your `sudo` password.
  - This process has a certain amount of noise, which can be ignored. [`Here's`](https://gist.github.com/noahwsmith/086d35d9ddb377102843052613bd744d) a trace which shows a "normal"/correct installation, for reference.
  - After this, there should be a functioning Islandora site available at [`https://islandora.traefik.me`](https://islandora.traefik.me)
- Import demo content by running `make demo_content` This will provide a few test objects and a uniform starting point for everyone. There is also a lot of noise in this command's output. As long as it ends like [this](https://gist.github.com/noahwsmith/737486029a1c4fc7d286f32ba250a5b7) you should be good to go...
    - If you ever need to reset your local, you can run `make clean local demo_content` to get back to this starting point.  
    - If you want to shut the containers down, run `make down`. If you've shut the containers down (intentionally or accidentally) you can bring everything back online with a `make up` command.
- You can interact with the site by shelling into the `drupal` container.
    - ex: `docker-compose exec -u nginx drupal bash` which will bring you to a command prompt at the drupal root. `composer` and `drush` commands will work from this location.
- You can gain access to User 1 by running `make login` and following the provided link to gain access to the Drupal UI. You can reset the password, or just hijack it every time you want to get in, either way.
- When you're done with your work, run `make config-export` which will dump out changed configs to the `codebase/config/sync` directory. Please check any changes into git alongside your changes to the `codebase/web/modules/custom/exercise1` directory.

## Exercise

This project contains a custom module housing a README that contains the instructions for the exercise.

- [Exercise: Make a self deposit form for the Repository Item content type](./codebase/web/modules/custom/exercise1/README.md)